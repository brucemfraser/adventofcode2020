class Seat:
    _row: int
    _col: int

    def __init__(self, row: int, col: int):
        self._row = row
        self._col = col

    @property
    def id(self) -> int:
        return (self._row * 8) + self._col

    def __str__(self):
        return str({'row': self._row, 'col': self._col, 'id': self.id})


class Parser:

    def parse(self, enc_seat: str) -> Seat:
        enc_row, enc_col = enc_seat[:7], enc_seat[7:]
        row, col = self.decode(enc_row), self.decode(enc_col)
        return Seat(row, col)

    @staticmethod
    def decode(enc_bin: str) -> int:
        binary = enc_bin \
            .replace('F', '0') \
            .replace('B', '1') \
            .replace('L', '0') \
            .replace('R', '1')
        return int(binary, base=2)


parser = Parser()

with open('boarding_passes.txt') as f:
    seats = [parser.parse(line.strip()) for line in f]

all_seat_ids = [seat.id for seat in seats]
min_seat_id = min(all_seat_ids)
max_seat_id = max(all_seat_ids)

# Problem 1
print(max_seat_id)

all_seat_ids_in_range = set(range(min_seat_id, max_seat_id))
missing_seat_ids = all_seat_ids_in_range.difference(all_seat_ids)

# Problem 2
print(missing_seat_ids)
