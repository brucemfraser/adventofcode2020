from abc import ABCMeta, abstractmethod
from re import compile, Pattern
from typing import Callable, NewType

Password = NewType('Password', str)


class Bound:
    _lower: int
    _upper: int

    def __init__(self, lower: int, upper: int):
        self._lower = lower
        self._upper = upper

    def __contains__(self, n: int) -> bool:
        return self._lower <= n <= self._upper


class IPolicy:
    __metaclass__ = ABCMeta

    @abstractmethod
    def is_valid(self, password: str) -> bool: raise NotImplementedError


class BoundPolicy(IPolicy):
    _requirement: str
    _bound: Bound

    def __init__(self, requirement: str, lower: int, upper: int):
        assert len(requirement) == 1
        self._requirement = requirement
        self._bound = Bound(lower, upper)

    def is_valid(self, password: str) -> bool:
        return password.count(self._requirement) in self._bound


class PositionPolicy(IPolicy):
    _requirement: str
    _lower: int
    _upper: int

    def __init__(self, requirement: str, lower: int, upper: int):
        assert len(requirement) == 1
        self._requirement = requirement
        self._lower = lower
        self._upper = upper

    def is_valid(self, password: str) -> bool:
        return PositionPolicy._matches(password, self._lower, self._requirement) \
               ^ PositionPolicy._matches(password, self._upper, self._requirement)

    @staticmethod
    def _matches(text: str, index: int, req: str) -> bool:
        return len(text) >= index and text[index - 1] == req


class Parser:
    _pattern: Pattern = compile(r'(?P<lower>\d+)-(?P<upper>\d+) (?P<requirement>.): (?P<password>.+)\s*')
    _policy_builder: Callable[[str, int, int], IPolicy]

    def __init__(self, policy_builder: Callable[[str, int, int], IPolicy]):
        self.policy_builder = policy_builder

    def parse(self, line: str) -> (IPolicy, Password):
        match = Parser._pattern.fullmatch(line)
        assert match is not None, f'no match: {line}'
        requirement = match.group('requirement')
        lower = int(match.group('lower'))
        upper = int(match.group('upper'))
        password = match.group('password')
        return self.policy_builder(requirement, lower, upper), Password(password)


# change for Problem 1 vs Problem 2
parser = Parser(PositionPolicy)

with open('passwords.txt') as f:
    parsed = [parser.parse(line) for line in f]

valid_count = sum(policy.is_valid(password) for (policy, password) in parsed)

print(valid_count)
