from functools import reduce
from itertools import groupby

with open('customs_declarations.txt') as f:
    lines = [line.strip() for line in f]
    group_lines = [list(group) for k, group in groupby(lines, bool) if k]
    group_answers = [[set(list(line)) for line in grp] for grp in group_lines]

# Problem 1: anyone/union
group_unique_answers = [reduce(lambda s1, s2: s1.union(s2), a) for a in group_answers]
print(sum([len(answers) for answers in group_unique_answers]))

# Problem 2: everyone/intersection
group_common_answers = [reduce(lambda s1, s2: s1.intersection(s2), a) for a in group_answers]
print(sum([len(answers) for answers in group_common_answers]))
