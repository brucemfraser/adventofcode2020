from functools import reduce


def product(iterable): return reduce(lambda x, y: x * y, iterable, 1)
