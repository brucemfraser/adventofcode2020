from functools import reduce
from re import Pattern, compile, Match
from typing import List, Optional, Iterator, Tuple, Set


class Bag:
    _colour: str

    def __init__(self, colour: str):
        self._colour = colour

    @property
    def colour(self) -> str:
        return self._colour

    def __hash__(self) -> int:
        return hash(self._colour)

    def __eq__(self, o: object) -> bool:
        return self.colour == o.colour if isinstance(o, Bag) else NotImplemented

    def __repr__(self) -> str:
        return f'{self._colour} bag(s)'


class Contents:
    _bag: Bag
    _count: int

    def __init__(self, bag: Bag, count: int):
        self._bag = bag
        self._count = count

    @property
    def bag(self) -> Bag:
        return self._bag

    @property
    def count(self) -> int:
        return self._count

    def __contains__(self, item):
        return item == self._bag

    def __repr__(self) -> str:
        return f'{self._count} {self._bag}'

    def __mul__(self, n):
        return Contents(self._bag, self._count * n)


class Rule:
    _container: Bag
    _contents: List[Contents]

    def __init__(self, container: Bag, contents: List[Contents]):
        self._container = container
        self._contents = contents

    @property
    def container(self) -> Bag:
        return self._container

    @property
    def contents(self) -> List[Contents]:
        return self._contents


class Graph:
    _rules: List[Rule]

    def __init__(self, rules: List[Rule]):
        self._rules = rules

    def parents(self, bag: Bag) -> Set[Bag]:
        return {rule.container for rule in self._rules if any(bag in c for c in rule.contents)}

    # assumes the graph is acyclic
    def ancestors(self, bag: Bag) -> Set[Bag]:
        parents = self.parents(bag)
        return parents.union(reduce(set.union, [self.ancestors(p) for p in parents], set()))

    def children(self, bag: Bag) -> List[Contents]:
        return next(rule.contents for rule in self._rules if rule.container == bag)

    def descendants(self, bag: Bag) -> List[Contents]:
        children = self.children(bag)
        return children + [desc * contents.count
                           for contents in children
                           for desc in self.descendants(contents.bag)]


class Parser:
    _container_pattern: Pattern = compile(r'^(?P<colour>.*) bags contain .*')
    _contents_pattern: Pattern = compile(r'(?:(?P<count>\d+) (?P<colour>[^,.]+)|no other) bags?[,.]')

    @staticmethod
    def parse(line: str) -> Rule:
        container_match: Optional[Match] = Parser._container_pattern.fullmatch(line)
        contents_matches: Iterator[Match] = Parser._contents_pattern.finditer(line)
        assert container_match, f'no container: {line}'
        bag: Bag = Bag(container_match.group('colour'))
        colours_and_counts: List[Tuple[str, str]] = [(match.group('colour'), match.group('count'))
                                                     for match in contents_matches if match is not None]
        contents: List[Contents] = [Contents(Bag(colour), int(count))
                                    for colour, count in colours_and_counts
                                    if colour is not None and count is not None]
        return Rule(bag, contents)


parser = Parser()

with open('luggage_rules.txt') as f:
    lines = [line.strip() for line in f]
    graph = Graph([parser.parse(line) for line in lines])

shiny_gold_bag = Bag('shiny gold')

# Problem 1
print(len(graph.ancestors(shiny_gold_bag)))

# Problem 2
print(sum([c.count for c in graph.descendants(shiny_gold_bag)]))
