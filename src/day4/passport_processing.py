from abc import abstractmethod, ABCMeta
from itertools import groupby
from re import compile, Pattern
from typing import Dict, Optional, Callable


class Passport:
    _fields: Dict[str, str]

    def __init__(self, fields: Dict[str, str]):
        self._fields = fields

    def __contains__(self, item) -> bool:
        return item in self._fields.keys()

    def __getitem__(self, item) -> Optional[str]:
        return self._fields.get(item, None)


class Parser:
    _pattern: Pattern = compile(r'(?P<key>\w+):(?P<value>\S+)')

    @staticmethod
    def parse(data: str) -> Passport:
        fields = Parser._pattern.findall(data)
        return Passport(dict(fields))


class PassportValidator:
    __metaclass__ = ABCMeta

    @abstractmethod
    def is_valid(self, passport: Passport) -> bool: raise NotImplementedError


class LenientValidator(PassportValidator):
    _required_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']  # excludes 'cid'

    def is_valid(self, passport: Passport) -> bool:
        return all([field in passport for field in LenientValidator._required_fields])


class StrictValidator(PassportValidator):
    _hcl_pattern: Pattern = compile(r'#[0-9a-f]{6}')
    _pid_pattern: Pattern = compile(r'[0-9]{9}')

    _field_reqs: Dict[str, Callable[[Optional[str]], bool]]

    def __init__(self):
        self._field_reqs = {
            'byr': self.byr_valid,
            'iyr': self.iyr_valid,
            'eyr': self.eyr_valid,
            'hgt': self.hgt_valid,
            'hcl': self.hcl_valid,
            'ecl': self.ecl_valid,
            'pid': self.pid_valid,
        }

    def is_valid(self, passport: Passport) -> bool:
        fields_and_reqs = [(passport[field_name], req) for field_name, req in self._field_reqs.items()]
        return all([field and req(field) for field, req in fields_and_reqs])

    @staticmethod
    def byr_valid(byr: str) -> bool:
        return 1920 <= int(byr) <= 2002 if byr.isdigit() else False

    @staticmethod
    def iyr_valid(iyr: str) -> bool:
        return 2010 <= int(iyr) <= 2020 if iyr.isdigit() else False

    @staticmethod
    def eyr_valid(eyr: str) -> bool:
        return 2020 <= int(eyr) <= 2030 if eyr.isdigit() else False

    @staticmethod
    def hgt_valid(hgt: str) -> bool:
        if len(hgt) <= 2:
            return False
        value, unit = hgt[:-2], hgt[-2:]
        if not value.isdigit():
            return False
        return 150 <= int(value) <= 193 if unit == 'cm' \
            else 59 <= int(value) <= 76 if unit == 'in' \
            else False

    @staticmethod
    def hcl_valid(hcl: str) -> bool:
        return StrictValidator._hcl_pattern.fullmatch(hcl) is not None

    @staticmethod
    def ecl_valid(ecl: str) -> bool:
        return ecl in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']

    @staticmethod
    def pid_valid(pid: str) -> bool:
        return StrictValidator._pid_pattern.fullmatch(pid) is not None


parser = Parser()

with open('passport_data.txt') as f:
    lines = [line.strip() for line in f]
    passport_data_lines = [list(group) for k, group in groupby(lines, bool) if k]
    passport_data = [' '.join(lines) for lines in passport_data_lines]
    passports = [parser.parse(p) for p in passport_data]

# change for Problem 1 vs Problem 2
validator: PassportValidator = StrictValidator()

valid = [validator.is_valid(passport) for passport in passports]

valid_passport_count = sum(valid)

print(valid_passport_count)
