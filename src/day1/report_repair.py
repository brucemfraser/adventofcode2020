from itertools import combinations

from util import product

# If you didn't know about itertools.combinations and implemented it yourself:
#
# def combinations_2(iterable):
#     return [
#         (x, y)
#         for i, x in enumerate(iterable)
#         for j, y in enumerate(iterable)
#         if i != j
#     ]
#
#
# def combinations_3(iterable):
#     return [
#         (x, y, z)
#         for i, x in enumerate(iterable)
#         for j, y in enumerate(iterable)
#         for k, z in enumerate(iterable)
#         if i != j and j != k
#     ]

# change for Problem 1 vs Problem 2
entry_count = 3

with open('expense_report.txt') as f:
    entries = [int(line) for line in f]

matching_entries = next(e for e in combinations(entries, entry_count) if sum(e) == 2020)

print(product(matching_entries))
