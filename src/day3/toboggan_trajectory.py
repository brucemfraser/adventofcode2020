from enum import Enum, unique
from typing import List, Optional

from util import product

Coordinate = (int, int)
Vector = (int, int)


@unique
class Feature(Enum):
    OPEN = '.'
    TREE = '#'


class Map:
    _rows: List[List[Feature]]

    def __init__(self, rows: List[List[Feature]]):
        self._rows = rows

    # top left is (0, 0)
    def feature_at(self, c: Coordinate) -> Optional[Feature]:
        (x, y) = c
        if y < 0 or len(self._rows) <= y:
            return None
        else:
            row = self._rows[y]
            return row[x % len(row)]


class Navigator:
    _map: Map

    def __init__(self, m: Map):
        self._map = m

    def generate_path(self, slope: Vector, start: Coordinate = (0, 0)) -> (Optional[Feature], Coordinate):
        (feature, location) = self.navigate(start, slope)
        while feature is not None:
            yield feature
            (feature, location) = self.navigate(location, slope)

    def navigate(self, src: Coordinate, by: Vector) -> (Optional[Feature], Coordinate):
        (src_x, src_y) = src
        (by_x, by_y) = by
        dest = (src_x + by_x, src_y + by_y)
        feature = self._map.feature_at(dest)
        return feature, dest


class Parser:
    @staticmethod
    def parse(line: str) -> List[Feature]:
        return [Feature(c) for c in line]


parser = Parser()

with open('map.txt') as f:
    map_rows = [parser.parse(line.rstrip()) for line in f]

navigator = Navigator(Map(map_rows))

# adjust for Problem 1 vs Problem 2
slopes = [
    (1, 1),
    (3, 1),
    (5, 1),
    (7, 1),
    (1, 2)
]
paths = [navigator.generate_path(slope) for slope in slopes]
tree_counts = [sum(f == Feature.TREE for f in path) for path in paths]

print(product(tree_counts))
